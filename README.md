### Compiled image for OrangePI hardware
  Built along the following bug report: [https://bugs.alpinelinux.org/issues/9826](https://bugs.alpinelinux.org/issues/9826)  
  Full dmesg: [https://gist.github.com/pgauret/efdde64840f8ac4081e159d9a8ffdb93](https://gist.github.com/pgauret/efdde64840f8ac4081e159d9a8ffdb93)  

```bash
...
[ 5.688051] calling tcp_congestion_default+0x0/0x2c 1
[ 5.693363] initcall tcp_congestion_default+0x0/0x2c returned 0 after 3 usecs
[ 5.700515] calling software_resume+0x0/0x264 1
[ 5.705305] initcall software_resume+0x0/0x264 returned -2 after 2 usecs
[ 5.712027] calling clear_boot_tracer+0x0/0x3c 1
[ 5.716903] initcall clear_boot_tracer+0x0/0x3c returned 0 after 0 usecs
[ 5.723617] calling clk_disable_unused+0x0/0xe8 1
```

  Just replace precompiled binary (zImage)  
  Steps to build using the multi_v7_defconfig  
```bash
wget https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-4.19.35.tar.xz
tar -xf linux-4.19.35.tar.xz
cd linux-4.19.35
make ARCH=arm MARCH=armv7 CFLAGS="-march=armv7-a -mfloat-abi=hard -mfpu=vfpv3-d16 -Os -pipe -fstack-protector-strong -fno-plt" CROSS_COMPILE=/usr/bin/arm-none-eabi- -j4 multi_v7_defconfig
make ARCH=arm MARCH=armv7 CFLAGS="-march=armv7-a -mfloat-abi=hard -mfpu=vfpv3-d16 -Os -pipe -fstack-protector-strong -fno-plt" CROSS_COMPILE=/usr/bin/arm-none-eabi- -j4
```

Linux kernel
============

There are several guides for kernel developers and users. These guides can
be rendered in a number of formats, like HTML and PDF. Please read
Documentation/admin-guide/README.rst first.

In order to build the documentation, use ``make htmldocs`` or
``make pdfdocs``.  The formatted documentation can also be read online at:

    https://www.kernel.org/doc/html/latest/

There are various text files in the Documentation/ subdirectory,
several of them using the Restructured Text markup notation.
See Documentation/00-INDEX for a list of what is contained in each file.

Please read the Documentation/process/changes.rst file, as it contains the
requirements for building and running the kernel, and information about
the problems which may result by upgrading your kernel.
